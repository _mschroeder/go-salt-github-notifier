package ci

import (
	"strings"
)

type Tags struct {
	Message string
	Skip    string
	Run     string
	File    string
	Pillar  string
	Mine    string
	Sync    string
}

func (t Tags) HasSkip() bool {
	return strings.Contains(t.Message, t.Skip)
}

func (t Tags) HasRun() bool {
	return strings.Contains(t.Message, t.Run)
}

func (t Tags) HasFile() bool {
	return strings.Contains(t.Message, t.File)
}

func (t Tags) HasPillar() bool {
	return strings.Contains(t.Message, t.Pillar)
}

func (t Tags) HasMine() bool {
	return strings.Contains(t.Message, t.Mine)
}

func (t Tags) HasSync() bool {
	return strings.Contains(t.Message, t.Sync)
}
