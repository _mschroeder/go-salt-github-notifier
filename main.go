package main

import (
	"fmt"
	"gitlab.com/_mschroeder/go-salt-github-notifier/ci"
	"gitlab.com/_mschroeder/go-salt-github-notifier/cli"
	"gitlab.com/_mschroeder/go-salt-github-notifier/notify"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
	"gopkg.in/go-playground/webhooks.v3"
	"gopkg.in/go-playground/webhooks.v3/github"
	"strconv"
)

var (
	port   = kingpin.Flag("port", "Port to listen on").PlaceHolder("8888").Envar("SN_PORT").Default("8888").Int()
	path   = kingpin.Flag("path", "Path to listen on").PlaceHolder("/webhooks").Envar("SN_PATH").Default("/webhooks").String()
	secret = kingpin.Flag("secret", "GitHub webhook secret").Envar("SN_SECRET").String()
	exec   = kingpin.Flag("exec-path", "Path to salt executables").PlaceHolder("/usr/bin").Envar("SN_EXEC_PATH").Default("/usr/bin").String()
	refs   = kingpin.Flag("refs", "GitHub Ref whitelist patterns").Envar("SN_REFS").Default("refs/heads/*").RegexpList()
	repos  = kingpin.Flag("repos", "GitHub Repository whitelist patterns").Envar("SN_REPOS").Default(".*").RegexpList()
	batch  = kingpin.Flag("batch", "Batch size to use when running salt commands").Envar("SN_BATCH").Default("20").Int()

	ciDisabled = kingpin.Flag("disable-ci", "Disable CI. If disabled you can still use CI tags to run tasks.").Envar("SN_DISABLE_CI").Default("false").Bool()
	ciSkip     = kingpin.Flag("ci-skip", "CI Skip Tag - Skips all processing").Envar("SN_CI_SKIP").Default("[sn-skip]").String()
	ciRun      = kingpin.Flag("ci-run", "CI Run Tag - Runs all processing").Envar("SN_CI_RUN").Default("[sn-run]").String()
	ciFile     = kingpin.Flag("ci-file", "CI File Tag - (salt-run fileserver.clear_cache & salt-run fileserver.update)").Envar("SN_CI_FILE").Default("[sn-file]").String()
	ciPillar   = kingpin.Flag("ci-pillar", "CI Pillar Tag - (salt-run git_pillar.update)").Envar("SN_CI_PILLAR").Default("[sn-pillar]").String()
	ciMine     = kingpin.Flag("ci-mine", "CI Mine Tag - (salt '*' mine.update)").Envar("SN_CI_MINE").Default("[sn-mine]").String()
	ciSync     = kingpin.Flag("ci-sync", "CI Sync Tag - (salt '*' saltutil.sync_all)").Envar("SN_CI_SYNC").Default("[sn-sync]").String()
)

func main() {
	kingpin.Version("0.0.3")
	kingpin.Parse()

	hook := github.New(&github.Config{Secret: *secret})
	hook.RegisterEvents(HandlePush, github.PushEvent)

	err := webhooks.Run(hook, ":"+strconv.Itoa(*port), *path)
	if err != nil {
		fmt.Println(err)
	}
}

// HandlePush handles GitHub Push events
func HandlePush(payload interface{}, header webhooks.Header) {

	fmt.Println("Handling Push")
	pl := payload.(github.PushPayload)

	if false == notify.ValidRef(refs, pl.Ref) {
		webhooks.DefaultLog.Info("Git ref did not match provided patterns")
		return
	}

	if false == notify.ValidRepo(repos, pl.Repository.FullName) {
		webhooks.DefaultLog.Info("Git repository did not match provided patterns")
		return
	}

	c := ci.Tags{
		Message: pl.HeadCommit.Message,
		Skip:    *ciSkip,
		Run:     *ciRun,
		File:    *ciFile,
		Pillar:  *ciPillar,
		Mine:    *ciMine,
		Sync:    *ciSync,
	}

	if c.HasSkip() {
		webhooks.DefaultLog.Info("CI Skipped")
		return
	}

	if *ciDisabled && c.HasRun() == false {
		webhooks.DefaultLog.Info("CI Disabled - No Override")
		return
	}

	if c.HasFile() || c.HasMine() || c.HasPillar() || c.HasSync() {
		webhooks.DefaultLog.Info("Running Subset of Tasks")
		salt := cli.NewSalt(*exec, *batch)
		if c.HasFile() {
			err := salt.FileCacheClear()
			err1 := salt.FileUpdate()
			if err != nil || err1 != nil {
				webhooks.DefaultLog.Info("Error synchronizing File Server")
			} else {
				webhooks.DefaultLog.Info("File Server synchronized")
			}
		}

		if c.HasPillar() {
			err := salt.PillarUpdate()
			if err != nil {
				webhooks.DefaultLog.Info("Error synchronizing Pillar")
			} else {
				webhooks.DefaultLog.Info("Pillar synchronized")
			}
		}

		if c.HasMine() {
			err := salt.MineUpdate()
			if err != nil {
				webhooks.DefaultLog.Info("Error synchronizing Mine")
			} else {
				webhooks.DefaultLog.Info("Mine synchronized")
			}
		}

		if c.HasSync() {
			err := salt.SyncAll()
			if err != nil {
				webhooks.DefaultLog.Info("Error synchronizing custom modules, states, beacons, grains, returners, output modules, renderers, and utils")
			} else {
				webhooks.DefaultLog.Info("Custom modules, states, beacons, grains, returners, output modules, renderers, and utils synchronized")
			}
		}

	} else {
		webhooks.DefaultLog.Info("Running All Tasks")
		salt := cli.NewSalt(*exec, *batch)
		err := salt.RunAll()

		if err != nil {
			webhooks.DefaultLog.Info("Error synchronizing All Tasks")
		} else {
			webhooks.DefaultLog.Info("All Tasks Complete")
		}
	}
}
